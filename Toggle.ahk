^!f:: ;control + alt + f 是启动热键
    ;toggle title bar window style
    ;WinSet, Style, ^0x800000, A 这是iglvzx方法
    ;WinSet, Style, ^0xC00000, A ;这是在国内weilaiqing那里看到的方法
    ;以下是Izzy Galvez的方法
    WinSet, Style, ^0xB00000, A ;toggle 0xB00000 on
    WinSet, Style, ^0x800000, A ;toggle fullscreen
    WinSet, Style, ^0x400000 ; hide dialog frame
    WinSet, Style, ^0x40000 ; hide thickframe/sizebox
    WinSet, Style, ^0xB00000, A ;toggle 0xB00000 back off
    WinSet, Transparent, 220, A ;这是设置窗口半透明的方法0为完全透明，255为不透明，推荐值220

    ;force redraw
    WinHide, A
    WinShow, A
return